use std::io::Cursor;

use arg_parsing::parse_args;
use conf::{RdlConf, get_default_folder};
use scraper::{Html, Selector};
use std::error::Error;

mod arg_parsing;
mod conf;

fn name_contains_all_patterns(link_name: &str, patterns: &Vec<String>) -> bool {
    for pattern in patterns.iter() {
        if ! link_name.contains(pattern) {
            return false;
        }
    }
    return true;
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    println!("( ͡° ͜ʖ ͡°)");
    let args = parse_args();
    let print_conf = args.is_present("configuration");
    let conf = RdlConf::new(args);

    if print_conf {
        RdlConf::create_default_cfg();
        println!("The configuration is registered in the file {}", get_default_folder());
        println!("{}", conf);
    }

    let username = match conf.user {
        Some(usr) => usr,
        None => String::new()
    };

    let password = match conf.password {
        Some(pass) => pass,
        None => String::new()
    };

    let base_url = match conf.base_url {
        Some(url) => url,
        None => return Err(format!("No base_url provided. Please give one by parameter ( -b ) or edit the configuration file {}", get_default_folder()))?
    };

    if conf.resource_patterns.len() == 0 {
        return Err(format!("No search pattern provided. Please use at least one by parameter or check your configuration file {}", get_default_folder()))?;
    }

    let creds = format!("{}:{}", username, password);
    let base64_creds = base64::encode(&creds);

    let client = reqwest::Client::new();
    let res = client
        .get(&base_url)
        .header("Authorization", "Basic ".to_owned() + &base64_creds)
        .send()
        .await?;

    if ! res.status().to_string().starts_with("2") {
        return Err(format!("The request to {} did not finished well : {:#?}", &base_url, &res))?
    }

    let document = Html::parse_document(&res.text().await?);
    let links = Selector::parse("a").unwrap();

    for link in document.select(&links) {
        let link_name = link.inner_html();
        if name_contains_all_patterns(&link_name, &conf.resource_patterns) {

            let new_link = format!("{}/{}", &base_url, &link_name);
            let res = client
                .get(new_link)
                .header("Authorization", "Basic ".to_owned() + &base64_creds)
                .send()
                .await?;
            if res.status() != 200 {
                panic!("{:#?}", res);
            }
            println!("Downloading file '{}'. Please wait...", link_name);
            let mut file = std::fs::File::create(link_name)?;
            let mut content = Cursor::new(res.bytes().await?);
            std::io::copy(&mut content, &mut file)?;
            break;
        }
    }

    Ok(())
}
