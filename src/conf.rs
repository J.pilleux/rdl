use clap::ArgMatches;
use serde::{Deserialize, Serialize};
use std::{
    env,
    fs::{self, create_dir, File},
    io::Write,
    path::Path, fmt::Display,
};

use colored::*;

const CFG_FILE_NAME: &str = "rdl.json";

pub fn get_default_folder() -> String {
    let cfg_home = match env::var("XDG_CONFIG_HOME") {
        Ok(path) => {
            let config_dir = format!("{}/rdl/", &path);
            if !Path::new(&config_dir).exists() {
                create_dir(&config_dir).expect("Cannot create directories");
            }
            config_dir + CFG_FILE_NAME
        }
        Err(..) => format!("{}/.{}", env::var("HOME").unwrap(), CFG_FILE_NAME),
    };
    cfg_home
}

fn dft_str() -> Option<String> {
    None
}

fn dft_patterns() -> Vec<String> {
    Vec::new()
}

#[derive(Deserialize, Serialize)]
pub struct RdlConf {
    #[serde(default = "dft_str")]
    pub user: Option<String>,
    #[serde(default = "dft_str")]
    pub password: Option<String>,
    #[serde(default = "dft_str")]
    pub base_url: Option<String>,
    #[serde(default = "dft_patterns")]
    pub resource_patterns: Vec<String>,
}

impl RdlConf {
    fn new_default() -> RdlConf {
        RdlConf {
            user: dft_str(),
            password: dft_str(),
            base_url: dft_str(),
            resource_patterns: dft_patterns(),
        }
    }
    pub fn new(args: ArgMatches) -> RdlConf {
        RdlConf::create_default_cfg();
        let mut conf = RdlConf::load_from_file();

        if let Some(user) = args.value_of("username") {
            conf.user = Some(String::from(user));
        }

        if let Some(password) = args.value_of("password") {
            conf.password = Some(String::from(password));
        }

        if let Some(base_url) = args.value_of("base_url") {
            conf.base_url = Some(String::from(base_url));
        }

        if let Some(patterns) = args.values_of("patterns") {
            conf.resource_patterns = patterns.map(String::from).collect();
        }

        conf.validate();
        conf
    }

    fn load_from_file() -> RdlConf {
        let path = get_default_folder();
        let content = fs::read_to_string(path).expect("Cannot read configuration from file");
        return serde_json::from_str(&content).expect("Cannot deserialize configuration file");
    }

    pub fn create_default_cfg() {
        let path = get_default_folder();
        if !Path::new(&path).exists() {
            let conf = RdlConf::new_default();
            let content =
                serde_json::to_string(&conf).expect("Cannot deserialize default configuration");
            let mut file = File::create(&path).expect("Cannot create file");
            file.write_all(content.as_bytes())
                .expect("Cannot write default file");
            println!("File creted at : {}", &path);
        }
    }

    fn validate(&self) {
        // TODO
    }
}

impl Display for RdlConf {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut ret = String::from("Current configuration :\n");
        let username = match &self.user {
            Some(usr) => String::from(usr),
            None => String::new()
        };

        let password = match &self.password {
            Some(pass) => String::from(pass),
            None => String::new()
        };

        let base_url = match &self.base_url {
            Some(url) => String::from(url),
            None => String::new()
        };

        ret.push_str(format!("\t{} : {}\n", "username".blue(), username).as_str());
        ret.push_str(format!("\t{} : {}\n", "password".blue(), password).as_str());
        ret.push_str(format!("\t{} : {}\n", "base_url".blue(), base_url).as_str());
        ret.push_str(format!("\t{} : {:#?}\n", "patterns".blue(), self.resource_patterns).as_str());
        write!(f, "{}", ret)
    }
}
