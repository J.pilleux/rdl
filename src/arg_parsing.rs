use clap::{App, Arg, ArgMatches};

const GLOBAL_HELP: &str = r#"A resource downloader utility written in rust.
The parameters of this command can be defined in a configuration file given by the -c parameter.
Parameters found in configuration file are overwirtten by the CLI arguments."#;

const PATTERN_HELP: &str = r#"Patterns to match when searching for a link to download.
Multiple pattern can be defined like this : rdl "search_pattern_1" "search_pattern_2" ...
All pattern must match to select a link to download"#;

pub fn parse_args() -> ArgMatches<'static> {
    App::new("rdl")
        .version("0.1.0")
        .author("Julien PILLEUX")
        .about(GLOBAL_HELP)
        .set_term_width(120)
        .arg(
            Arg::with_name("patterns")
                .value_name("patterns")
                .help(&PATTERN_HELP)
                .multiple(true),
        )
        .arg(
            Arg::with_name("username")
                .short("u")
                .long("username")
                .takes_value(true)
                .value_name("USER")
                .help("The username to use into the HTTP request."),
        )
        .arg(
            Arg::with_name("password")
                .short("p")
                .long("password")
                .takes_value(true)
                .value_name("PASS")
                .help("The password to use into the HTTP request."),
        )
        .arg(
            Arg::with_name("base_url")
                .short("b")
                .long("base_url")
                .takes_value(true)
                .value_name("URL")
                .help("The url where to search the given pattern."),
        ).arg(
            Arg::with_name("configuration")
                .short("c")
                .long("configuration")
                .takes_value(false)
                .help("Show the path where the configuration is saved.")
            )
        .get_matches()
}
