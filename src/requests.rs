use crate::conf::RdlConf;
use hyper::{Client, Uri};

pub async fn requests(conf: RdlConf) {
    let client = Client::new();
    let uri: Uri = "https://www.rust-lang.org/"
        .parse()
        .unwrap();
    match client.get(uri).await {
        Ok(res) => println!("Response: {}", res.status()),
        Err(err) => println!("Error: {}", err),
    }
}
